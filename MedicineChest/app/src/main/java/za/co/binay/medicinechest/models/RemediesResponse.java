package za.co.binay.medicinechest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RemediesResponse {

    @SerializedName("remedies")
    @Expose
    public List<RemedyModel> remedies;
}
