package za.co.binay.medicinechest.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import za.co.binay.medicinechest.R;
import za.co.binay.medicinechest.models.PrescriptionModel;

public class PrescriptionAdapter extends RecyclerView.Adapter<PrescriptionAdapter.ViewHolder> {
    private List<PrescriptionModel> prescriptionModelList;
    private Context context;

    public PrescriptionAdapter(List<PrescriptionModel> DataSet, Context context){
        this.prescriptionModelList = DataSet;
        this.context = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvMedicine, tvDosage, tvRefills, tvDateCreated;

        ViewHolder(View view) {
            super(view);
            tvMedicine = view.findViewById(R.id.tvMedicationAndStrength);
            tvDosage = view.findViewById(R.id.tvAmountAndFrequency);
            tvRefills = view.findViewById(R.id.tvRefills);
            tvDateCreated = view.findViewById(R.id.tvDate);
        }
    }

    @Override
    public PrescriptionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.prescription_row, parent, false);

        return new PrescriptionAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PrescriptionAdapter.ViewHolder holder, int position) {
        holder.tvMedicine.setText(prescriptionModelList.get(position).medicine);
        holder.tvDosage.setText(prescriptionModelList.get(position).dosage);

        if (prescriptionModelList.get(position).refills == 1){
            holder.tvRefills.setText(prescriptionModelList.get(position).refills + " refill left");
        } else {
            holder.tvRefills.setText(prescriptionModelList.get(position).refills + " refills left");
        }

        holder.tvDateCreated.setText(prescriptionModelList.get(position).datecreated);
    }

    @Override
    public int getItemCount() {
        return prescriptionModelList.size();
    }
}
