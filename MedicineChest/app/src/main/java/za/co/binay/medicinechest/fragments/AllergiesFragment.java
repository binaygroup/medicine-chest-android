package za.co.binay.medicinechest.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.binay.medicinechest.R;
import za.co.binay.medicinechest.adapters.AllergyAdapter;
import za.co.binay.medicinechest.helpers.SessionHelper;
import za.co.binay.medicinechest.models.AllergiesResponse;
import za.co.binay.medicinechest.models.AllergyModel;
import za.co.binay.medicinechest.rest.ApiClient;
import za.co.binay.medicinechest.rest.ApiInterface;

public class AllergiesFragment extends Fragment {

    View v;
    private RecyclerView rvAllergies;
    private List<AllergyModel> allergyModelList;
    private SessionHelper session;
    private AllergyAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_allergies, container, false);
        rvAllergies = v.findViewById(R.id.rvAllergies);
        adapter = new AllergyAdapter(allergyModelList);
        rvAllergies.setLayoutManager(new LinearLayoutManager(getActivity()));
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        allergyModelList = new ArrayList<>();
        session = new SessionHelper(getActivity());

        getData();
    }


    private void getData() {
        String filter = "uid,eq," + session.getUID();

        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);

        Call<AllergiesResponse> call = service.LoadAllergies(filter);

        call.enqueue(new Callback<AllergiesResponse>() {
            @Override
            public void onResponse(Call<AllergiesResponse> call, Response<AllergiesResponse> response) {
                if (response.isSuccessful()){
                    AllergiesResponse allergiesResponse = response.body();

                    if (allergiesResponse != null){
                        adapter = new AllergyAdapter(allergiesResponse.allergies);
                        rvAllergies.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<AllergiesResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), "Couldn't fetch Allergies", Toast.LENGTH_SHORT).show();
            }
        });
    }
}