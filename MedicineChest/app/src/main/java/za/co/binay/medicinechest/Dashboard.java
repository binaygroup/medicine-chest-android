package za.co.binay.medicinechest;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.binay.medicinechest.helpers.SessionHelper;
import za.co.binay.medicinechest.models.DashboardResponse;
import za.co.binay.medicinechest.rest.ApiClient;
import za.co.binay.medicinechest.rest.ApiInterface;

public class Dashboard extends AppCompatActivity {

    private ActionBar mActionBar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private LinearLayout mPrescriptions, mRemedies, mHistory, mAlerts;
    private SessionHelper session;
    private TextView tvPrescriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        // Get a support ActionBar corresponding to this toolbar
        mActionBar = getSupportActionBar();

        if (mActionBar != null)
        {
            // Set the Title for this toolbar
            mActionBar.setTitle("Dashboard");

            // Enable the Up button
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        drawerLayout = findViewById(R.id.activity_dashboard);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.Open, R.string.Closed);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        session = new SessionHelper(this);

        tvPrescriptions = findViewById(R.id.tvPrescriptions);

        navigationView = findViewById(R.id.nv);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id)
                {
                    case R.id.logout:
                        session.endSession();
                        startActivity(new Intent(Dashboard.this, MainActivity.class));
                        Dashboard.this.finish();
                    default:
                        return true;
                }
            }
        });

        mPrescriptions = findViewById(R.id.layPrescriptions);
        mPrescriptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this, Prescriptions.class));
            }
        });

        mRemedies = findViewById(R.id.layTips);
        mRemedies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this, HomeRemedies.class));
            }
        });

        mHistory = findViewById(R.id.layHistory);
        mHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this, MedicalHistory.class));
            }
        });

//        mAlerts = findViewById(R.id.layAlerts);
//        mAlerts.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(Dashboard.this, AlertsActivity.class));
//            }
//        });

        fetchData();

    }

    private void fetchData() {
        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);

        Call<DashboardResponse> call = service.LoadDashboard(session.getUID());

        call.enqueue(new Callback<DashboardResponse>() {
            @Override
            public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                if (response.isSuccessful()){
                    if (!response.body().error){
                        DashboardResponse dashboard = response.body();

                        populateDashboard(dashboard.prescriptions);

                    }

                } else {
                    Log.e("RETROFIT", "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<DashboardResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void populateDashboard(String prescriptions) {
        tvPrescriptions.setText(prescriptions);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(toggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }
}
