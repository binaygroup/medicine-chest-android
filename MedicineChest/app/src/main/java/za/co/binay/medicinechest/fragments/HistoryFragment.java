package za.co.binay.medicinechest.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.binay.medicinechest.R;
import za.co.binay.medicinechest.adapters.AllergyAdapter;
import za.co.binay.medicinechest.adapters.HistoryAdapter;
import za.co.binay.medicinechest.helpers.SessionHelper;
import za.co.binay.medicinechest.models.AllergiesResponse;
import za.co.binay.medicinechest.models.AllergyModel;
import za.co.binay.medicinechest.models.HistoryModel;
import za.co.binay.medicinechest.models.HistoryResponse;
import za.co.binay.medicinechest.rest.ApiClient;
import za.co.binay.medicinechest.rest.ApiInterface;

public class HistoryFragment extends Fragment {

    View v;
    private RecyclerView rvHistory;
    private List<HistoryModel> historyModelList;
    private SessionHelper session;
    private HistoryAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_history, container, false);
        rvHistory = v.findViewById(R.id.rvHistory);
        adapter = new HistoryAdapter(historyModelList);
        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        historyModelList = new ArrayList<>();
        session = new SessionHelper(getActivity());

        getData();
    }


    private void getData() {
        String filter = "uid,eq," + session.getUID();

        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);

        Call<HistoryResponse> call = service.LoadHistory(filter);

        call.enqueue(new Callback<HistoryResponse>() {
            @Override
            public void onResponse(Call<HistoryResponse> call, Response<HistoryResponse> response) {
                if (response.isSuccessful()){
                    HistoryResponse historyResponse = response.body();

                    if (historyResponse != null){
                        adapter = new HistoryAdapter(historyResponse.medicalhistory);
                        rvHistory.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<HistoryResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), "Couldn't fetch Medical History", Toast.LENGTH_SHORT).show();
            }
        });
    }
}