package za.co.binay.medicinechest.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import za.co.binay.medicinechest.R;
import za.co.binay.medicinechest.models.AllergyModel;

public class AllergyAdapter extends RecyclerView.Adapter<AllergyAdapter.MyViewHolder> {

    private List<AllergyModel> allergyModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvAllergy, tvDiagnosisDate, tvMedication;

        public MyViewHolder(View view) {
            super(view);
            tvAllergy = view.findViewById(R.id.tvAllergy);
            tvDiagnosisDate = view.findViewById(R.id.tvDiagnosisDate);
            tvMedication = view.findViewById(R.id.tvMedication);
        }
    }


    public AllergyAdapter(List<AllergyModel> allergyList) {
        this.allergyModelList = allergyList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.allergy_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvAllergy.setText(allergyModelList.get(position).allergy);
        holder.tvDiagnosisDate.setText(allergyModelList.get(position).diagnosis_date);
        holder.tvMedication.setText(allergyModelList.get(position).medication);
    }

    @Override
    public int getItemCount() {
        return allergyModelList.size();
    }
}