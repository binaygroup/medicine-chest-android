package za.co.binay.medicinechest.helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationHelper {

    private Pattern passwordPattern;

    private Pattern personPattern;

    private Matcher matcher;

    private static final String PERSON_PATTERN = "^[\\p{L} .'-]+$";

    private static final String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40})";

    public ValidationHelper() {
        passwordPattern = Pattern.compile(PASSWORD_PATTERN);
        personPattern = Pattern.compile(PERSON_PATTERN);
    }

    public boolean isValidPassword(final String password) {
        matcher = passwordPattern.matcher(password);
        return matcher.matches();
    }

    public boolean isValidEmail(final CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean isValidPerson(final String names){
        matcher = personPattern.matcher(names);
        return matcher.matches();
    }

    public boolean isMatchPasswords(final String password1, final String password2){
        return password2.equals(password1);
    }

    public boolean isValidPhone(String phoneNo) {
        //validate phone numbers of format "1234567890"
        if (phoneNo.matches("\\d{10}")) return true;
            //validating phone number with -, . or spaces
        else if(phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
            //validating phone number with extension length from 3 to 5
        else if(phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
            //validating phone number where area code is in braces ()
        else if(phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
            //return false if nothing matches the input
        else return false;

    }
}
