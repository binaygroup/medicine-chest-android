package za.co.binay.medicinechest.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import za.co.binay.medicinechest.R;
import za.co.binay.medicinechest.models.AllergyModel;
import za.co.binay.medicinechest.models.HistoryModel;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private List<HistoryModel> historyModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIllness, tvDiagnosisDate, tvMedication;

        public MyViewHolder(View view) {
            super(view);
            tvIllness = view.findViewById(R.id.tvCondition);
            tvDiagnosisDate = view.findViewById(R.id.tvDiagnosisDate);
            tvMedication = view.findViewById(R.id.tvTreatment);
        }
    }


    public HistoryAdapter(List<HistoryModel> historyList) {
        this.historyModelList = historyList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvIllness.setText(historyModelList.get(position).illness);
        holder.tvDiagnosisDate.setText(historyModelList.get(position).diagnosis_date);
        holder.tvMedication.setText(historyModelList.get(position).medication);
    }

    @Override
    public int getItemCount() {
        return historyModelList.size();
    }
}