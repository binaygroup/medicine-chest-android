package za.co.binay.medicinechest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.binay.medicinechest.helpers.DialogHelper;
import za.co.binay.medicinechest.helpers.SessionHelper;
import za.co.binay.medicinechest.helpers.ValidationHelper;
import za.co.binay.medicinechest.models.RegisterModel;
import za.co.binay.medicinechest.models.RegisterResponse;
import za.co.binay.medicinechest.models.UserModel;
import za.co.binay.medicinechest.rest.ApiClient;
import za.co.binay.medicinechest.rest.ApiInterface;

public class Register extends AppCompatActivity {

    ValidationHelper validator;
    DialogHelper prompter;
    SessionHelper session;

    Button btnRegister;
    EditText etNames, etSurname, etEmail, etPhone, etPassword, etConfirmPassword;

    RegisterModel registerModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etNames = findViewById(R.id.etFullNames);
        etSurname = findViewById(R.id.etSurname);
        etEmail = findViewById(R.id.etEmail);
        etPhone = findViewById(R.id.etPhone);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);

        validator = new ValidationHelper();
        prompter = new DialogHelper(this);
        session = new SessionHelper(this);

        registerModel = new RegisterModel();

        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buildModel();
                if (isValidModel()){
                    registerUser();
                }
            }
        });
    }

    private boolean isValidModel(){
        if (isCompleteModel()) {
            if (validator.isValidPerson(registerModel.names)) {
                if (validator.isValidPerson(registerModel.surname)) {
                    if (validator.isValidEmail(registerModel.email)) {
                        if (validator.isValidPhone(registerModel.phone)) {
                            if (validator.isValidPassword(registerModel.password)) {
                                if (validator.isMatchPasswords(registerModel.password, etConfirmPassword.getText().toString())) {
                                    return true;
                                } else {
                                    prompter.showDialog("Password Unmatched", "Passwords entered do not match");
                                    return false;
                                }
                            } else {
                                prompter.showDialog("Invalid Password", "Password should be between 8 and 40 characters long, contain at least one digit, contain at least one lower case character, contain at least one upper case character, and contain at least on special character from [ @ # $ % ! . ]");
                                return false;
                            }
                        } else {
                            prompter.showDialog("Invalid Phone", "Phone Number entered is invalid");
                            return false;
                        }
                    } else {
                        prompter.showDialog("Invalid Email", "Email Address entered is invalid");
                        return false;
                    }
                } else {
                    prompter.showDialog("Invalid Person", "Surname entered is invalid");
                    return false;
                }
            } else {
                prompter.showDialog("Invalid Person", "Full Names entered are invalid");
                return false;
            }
        } else {
            prompter.showDialog("User Invalid", "Please complete all fields to register");
            return false;
        }
    }

    private boolean isCompleteModel(){
        return !registerModel.names.equals("") && !registerModel.surname.equals("") && !registerModel.email.equals("") && !registerModel.phone.equals("") && !registerModel.password.equals("");
    }

    private void buildModel() {
        registerModel.names = etNames.getText().toString();
        registerModel.surname = etSurname.getText().toString();
        registerModel.email = etEmail.getText().toString();
        registerModel.phone = etPhone.getText().toString();
        registerModel.password = etPassword.getText().toString();
    }

    private void registerUser() {
        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);

        Call<RegisterResponse> call = service.RegisterUser(registerModel.names, registerModel.surname, registerModel.email, registerModel.phone, registerModel.password);

        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.isSuccessful()){
                    if (!response.body().error){
                        UserModel user = response.body().user;

                        session.startSession(user.id);

                        Toast.makeText(Register.this, "Registration Successful", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(Register.this, Dashboard.class));
                        Register.this.finish();
                    } else {
                        prompter.showDialog("Registration Error", response.body().message);
                    }

                } else {
                    Log.e("RETROFIT", "onResponse: " + response.message());
                    prompter.showDialog("Oops!", "Something went wrong.\nRegistration was unsuccessful");
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                t.printStackTrace();
                prompter.showDialog("Oops!", "Something went wrong.\nPlease try again later.");
            }
        });
    }
}
