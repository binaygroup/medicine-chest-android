package za.co.binay.medicinechest.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrescriptionResponse {

    @SerializedName("prescriptions")
    @Expose
    public List<PrescriptionModel> prescriptions;
}