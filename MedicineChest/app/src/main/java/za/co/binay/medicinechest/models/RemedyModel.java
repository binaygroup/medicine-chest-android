package za.co.binay.medicinechest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RemedyModel {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("illness")
    @Expose
    public String illness;
    @SerializedName("remedy")
    @Expose
    public String remedy;
    @SerializedName("uid")
    @Expose
    public int uid;
    @SerializedName("datecreated")
    @Expose
    public String datecreated;
}
