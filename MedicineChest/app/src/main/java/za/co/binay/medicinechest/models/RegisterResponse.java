package za.co.binay.medicinechest.models;

public class RegisterResponse {
    public boolean error;
    public String message;
    public UserModel user;
}
