package za.co.binay.medicinechest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllergiesResponse {

    @SerializedName("allergies")
    @Expose
    public List<AllergyModel> allergies;
}