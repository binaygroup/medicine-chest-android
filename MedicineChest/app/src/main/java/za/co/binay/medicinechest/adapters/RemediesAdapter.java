package za.co.binay.medicinechest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import za.co.binay.medicinechest.R;
import za.co.binay.medicinechest.models.RemedyModel;

public class RemediesAdapter extends RecyclerView.Adapter<RemediesAdapter.ViewHolder> {
    private List<RemedyModel> remedyModelList;

    public RemediesAdapter(List<RemedyModel> DataSet){
        this.remedyModelList = DataSet;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvIllness, tvRemedy;

        ViewHolder(View view) {
            super(view);
            tvIllness = view.findViewById(R.id.tvIllness);
            tvRemedy = view.findViewById(R.id.tvRemedy);
        }
    }

    @Override
    public RemediesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.remedy_row, parent, false);

        return new RemediesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RemediesAdapter.ViewHolder holder, int position) {
        holder.tvIllness.setText(remedyModelList.get(position).illness);
        holder.tvRemedy.setText(remedyModelList.get(position).remedy);
    }

    @Override
    public int getItemCount() {
        return remedyModelList.size();
    }
}
