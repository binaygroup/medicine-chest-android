package za.co.binay.medicinechest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrescriptionModel {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("medicine")
    @Expose
    public String medicine;
    @SerializedName("dosage")
    @Expose
    public String dosage;
    @SerializedName("refills")
    @Expose
    public int refills;
    @SerializedName("uid")
    @Expose
    public int uid;
    @SerializedName("datecreated")
    @Expose
    public String datecreated;
}