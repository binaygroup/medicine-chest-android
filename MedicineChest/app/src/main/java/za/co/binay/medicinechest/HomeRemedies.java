package za.co.binay.medicinechest;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.binay.medicinechest.adapters.RemediesAdapter;
import za.co.binay.medicinechest.helpers.SessionHelper;
import za.co.binay.medicinechest.models.RemediesResponse;
import za.co.binay.medicinechest.rest.ApiClient;
import za.co.binay.medicinechest.rest.ApiInterface;

public class HomeRemedies extends AppCompatActivity {

    private ActionBar mActionBar;
    private SessionHelper session;
    private RecyclerView rvRemedies;
    private RemediesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_remedies);

        // Get a support ActionBar corresponding to this toolbar
        mActionBar = getSupportActionBar();

        if (mActionBar != null)
        {
            // Set the Title for this toolbar
            mActionBar.setTitle("Home Remedies");

            // Enable the Up button
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        session = new SessionHelper(this);

        rvRemedies = findViewById(R.id.rvRemedies);

        rvRemedies.setLayoutManager(new LinearLayoutManager(this));

        populateRemedies();
    }

    private void populateRemedies() {
        String filter = "uid,eq,5";

        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);

        Call<RemediesResponse> call = service.LoadRemedies(filter);

        call.enqueue(new Callback<RemediesResponse>() {
            @Override
            public void onResponse(Call<RemediesResponse> call, Response<RemediesResponse> response) {
                if (response.isSuccessful()){
                    RemediesResponse remediesResponse = response.body();

                    if (remediesResponse != null){
                        adapter = new RemediesAdapter(remediesResponse.remedies);
                        rvRemedies.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<RemediesResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(HomeRemedies.this, "Couldn't fetch Remedies", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
