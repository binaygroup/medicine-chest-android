package za.co.binay.medicinechest;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import za.co.binay.medicinechest.adapters.TabAdapter;
import za.co.binay.medicinechest.fragments.AllergiesFragment;
import za.co.binay.medicinechest.fragments.HistoryFragment;

public class MedicalHistory extends AppCompatActivity {

    private ActionBar mActionBar;
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_history);

        // Get a support ActionBar corresponding to this toolbar
        mActionBar = getSupportActionBar();

        if (mActionBar != null)
        {
            // Set the Title for this toolbar
            mActionBar.setTitle("Medical History");

            // Enable the Up button
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mActionBar.setElevation(0);
        }

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);

        adapter = new TabAdapter(getSupportFragmentManager());

        adapter.addFragment(new AllergiesFragment(), "Allergies");
        adapter.addFragment(new HistoryFragment(), "History");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
