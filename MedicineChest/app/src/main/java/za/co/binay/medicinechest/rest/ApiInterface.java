package za.co.binay.medicinechest.rest;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import za.co.binay.medicinechest.models.AllergiesResponse;
import za.co.binay.medicinechest.models.DashboardResponse;
import za.co.binay.medicinechest.models.HistoryResponse;
import za.co.binay.medicinechest.models.LoginModel;
import za.co.binay.medicinechest.models.LoginResponse;
import za.co.binay.medicinechest.models.PrescriptionModel;
import za.co.binay.medicinechest.models.PrescriptionResponse;
import za.co.binay.medicinechest.models.RegisterModel;
import za.co.binay.medicinechest.models.RegisterResponse;
import za.co.binay.medicinechest.models.RemediesResponse;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("medichest/api/Api.php?apicall=signup")
    Call<RegisterResponse> RegisterUser(@Field("names") String names, @Field("surname") String surname, @Field("email") String email, @Field("phone") String phone, @Field("password") String password);

    @FormUrlEncoded
    @POST("medichest/api/Api.php?apicall=login")
    Call<LoginResponse> LoginUser(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("medichest/api/Api.php?apicall=dashboard")
    Call<DashboardResponse> LoadDashboard(@Field("uid") Integer uid);

    @GET("medichest/api.php/prescriptions?transform=1")
    Call<PrescriptionResponse> LoadPrescriptions(@Query("filter") String filter);

    @GET("medichest/api.php/remedies?transform=1")
    Call<RemediesResponse> LoadRemedies(@Query("filter") String filter);

    @GET("medichest/api.php/allergies?transform=1")
    Call<AllergiesResponse> LoadAllergies(@Query("filter") String filter);

    @GET("medichest/api.php/medicalhistory?transform=1")
    Call<HistoryResponse> LoadHistory(@Query("filter") String filter);
}

