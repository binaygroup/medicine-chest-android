package za.co.binay.medicinechest.models;

public class LoginResponse {
    public boolean error;
    public String message;
    public UserModel user;
}
