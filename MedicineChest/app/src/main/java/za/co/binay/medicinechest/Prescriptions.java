package za.co.binay.medicinechest;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.binay.medicinechest.adapters.PrescriptionAdapter;
import za.co.binay.medicinechest.helpers.SessionHelper;
import za.co.binay.medicinechest.models.PrescriptionModel;
import za.co.binay.medicinechest.models.PrescriptionResponse;
import za.co.binay.medicinechest.rest.ApiClient;
import za.co.binay.medicinechest.rest.ApiInterface;

public class Prescriptions extends AppCompatActivity {

    private ActionBar mActionBar;
    private SessionHelper session;
    private RecyclerView rvPrescriptions;
    PrescriptionAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescriptions);

        // Get a support ActionBar corresponding to this toolbar
        mActionBar = getSupportActionBar();

        if (mActionBar != null)
        {
            // Set the Title for this toolbar
            mActionBar.setTitle("Prescriptions");

            // Enable the Up button
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        session = new SessionHelper(this);

        rvPrescriptions = findViewById(R.id.rvPrescriptions);

        rvPrescriptions.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        populatePrescriptions();

    }

    private void populatePrescriptions() {

        String filter = "uid,eq," + session.getUID().toString();

        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);

        Call<PrescriptionResponse> call = service.LoadPrescriptions(filter);

        call.enqueue(new Callback<PrescriptionResponse>() {
            @Override
            public void onResponse(Call<PrescriptionResponse> call, Response<PrescriptionResponse> response) {

                if (response.isSuccessful()){
                    PrescriptionResponse prescriptionResponse = response.body();

                    if (prescriptionResponse != null){
                        adapter = new PrescriptionAdapter(prescriptionResponse.prescriptions, getApplicationContext());
                        rvPrescriptions.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<PrescriptionResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(Prescriptions.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
