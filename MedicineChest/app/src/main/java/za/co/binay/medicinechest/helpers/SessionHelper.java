package za.co.binay.medicinechest.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SessionHelper {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public SessionHelper(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void startSession(int uid){
        editor = preferences.edit();
        editor.putInt("uid", uid);
        editor.apply();
    }

    public Integer getUID(){
        return preferences.getInt("uid", 0);
    }

    public void endSession(){
        editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public boolean isActive() {
        return preferences.getInt("uid", 0) != 0;
    }
}
