package za.co.binay.medicinechest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HistoryModel {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("illness")
    @Expose
    public String illness;
    @SerializedName("diagnosis_date")
    @Expose
    public String diagnosis_date;
    @SerializedName("medication")
    @Expose
    public String medication;
    @SerializedName("uid")
    @Expose
    public int uid;
    @SerializedName("datecreated")
    @Expose
    public String datecreated;
}
