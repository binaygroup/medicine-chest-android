package za.co.binay.medicinechest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.binay.medicinechest.helpers.DialogHelper;
import za.co.binay.medicinechest.helpers.SessionHelper;
import za.co.binay.medicinechest.helpers.ValidationHelper;
import za.co.binay.medicinechest.models.LoginModel;
import za.co.binay.medicinechest.models.LoginResponse;
import za.co.binay.medicinechest.models.UserModel;
import za.co.binay.medicinechest.rest.ApiClient;
import za.co.binay.medicinechest.rest.ApiInterface;

public class MainActivity extends AppCompatActivity {

    Button btnRegister, btnSignIn;
    EditText etEmailAddress, etPassword;
    Intent intent;

    SessionHelper session;
    ValidationHelper validator;
    DialogHelper prompter;

    LoginModel loginModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = new SessionHelper(this);

        if (session.isActive()){
            startActivity(new Intent(this, Dashboard.class));
            this.finish();
        }

        loginModel = new LoginModel();

        btnRegister = findViewById(R.id.btnShowRegister);
        btnSignIn = findViewById(R.id.btnSignIn);

        etEmailAddress = findViewById(R.id.etLoginEmail);
        etPassword = findViewById(R.id.etLoginPassword);

        validator = new ValidationHelper();
        prompter = new DialogHelper(this);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            buildModel();
            if (isValidModel()){
                loginUser();
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, Register.class);
                startActivity(intent);
            }
        });

    }

    private void loginUser() {
        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);

        Call<LoginResponse> call = service.LoginUser(loginModel.email, loginModel.password);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()){
                    if (!response.body().error){
                        UserModel user = response.body().user;

                        session.startSession(user.id);

                        Toast.makeText(MainActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(MainActivity.this, Dashboard.class));
                        MainActivity.this.finish();
                    } else {
                        prompter.showDialog("Login Error", response.body().message);
                    }

                } else {
                    Log.e("RETROFIT", "onResponse: " + response.message());
                    prompter.showDialog("Oops!", "Something went wrong.\nLogin was unsuccessful");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                t.printStackTrace();
                prompter.showDialog("Oops!", "Something went wrong.\nPlease try again later.");
            }
        });
    }

    private void buildModel() {
        loginModel.email = etEmailAddress.getText().toString();
        loginModel.password = etPassword.getText().toString();
    }

    private boolean isValidModel(){
        if (isCompleteModel()) {
            if (validator.isValidEmail(loginModel.email)) {
                    if (validator.isValidPassword(loginModel.password)) {
                        return true;
                    } else {
                        prompter.showDialog("Invalid Password", "Password should be between 8 and 40 characters long, contain at least one digit, contain at least one lower case character, contain at least one upper case character, and contain at least on special character from [ @ # $ % ! . ]");
                        return false;
                    }
            } else {
                prompter.showDialog("Invalid Email", "Email Address entered is invalid");
                return false;
            }
        } else {
            prompter.showDialog("User Invalid", "Please complete all fields to login");
            return false;
        }
    }

    private boolean isCompleteModel(){
        return !loginModel.email.equals("") && !loginModel.password.equals("");
    }

}
